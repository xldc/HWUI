
package com.hw.droid.hwcatalog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import hwdroid.app.HWActivity;

import java.util.Calendar;

public class CommonControl extends HWActivity {
    private ProgressBar mProgressBar;
    private Spinner mSpinner;
    private static final String[] mDropDownItems={"aaa","bbb","ccc","ddd"}; 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.showBackKey(true);
        setActivityContentView(R.layout.commoncontrol);
        mProgressBar = (ProgressBar) findViewById(R.id.pro);
        mProgressBar.setIndeterminate(true);
        
        DatePicker datePicker=(DatePicker)findViewById(R.id.datePicker);
        datePicker.setCalendarViewShown(false);

        mSpinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String>  adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,mDropDownItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);

        TextView txt = (TextView) this.findViewById(R.id.txt1);
        txt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
            }
        });

        Button btn = (Button) this.findViewById(R.id.btn1);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Calendar calendar = Calendar.getInstance();  
                
                DatePickerDialog.OnDateSetListener dateListener =  
                        new DatePickerDialog.OnDateSetListener() { 
                            @Override 
                            public void onDateSet(DatePicker datePicker,  
                                    int year, int month, int dayOfMonth) { 
                                
                            } 
                        };
                        
                DatePickerDialog  dialog = new DatePickerDialog(CommonControl.this, 
                    dateListener, 
                    calendar.get(Calendar.YEAR), 
                    calendar.get(Calendar.MONTH), 
                    calendar.get(Calendar.DAY_OF_MONTH)); 
                
                dialog.show();
            }
        });
    }

}
